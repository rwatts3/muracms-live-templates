MURA CMS Based Live Templates for WebStorm and Intellij Idea
============================================================

### Contributors

[Ryan Watts](mailto:ryandwatts@gmail.com)

### Instructions

Download or clone the repository. Take the MURA CMS.xml file and place
it in your Intellij IDEA / Webstorm, templates folder. Restart the IDE.
If you have any questions please contact [Ryan
Watts](mailto:ryandwatts@gmail.com)

### Contents & Features

-   Template Variables for Mura CMS
-   HTML\_head.cfm
-   Document Body
-   CSS Hooks

### Additional Information & Resources

[Mura CMS Template
Variables](http://docs.getmura.com/v6/front-end/template-variables/)

### Version Information & Updates

Current Version **0.1**

0.1

-   Added html\_head.cfm Template Variables to Live Templates

